#include "heapsort.h"
#include "ui_heapsort.h"
#include <QApplication>
#include <QThread>
#include <QMutex>
#include <iostream>
#include <time.h>
using namespace std;

/* ---------------------------------------------------------------------- */

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

/* ---------------------------------------------------------------------- */

const int N = 100;

int a[N];

inline void swap (int & x, int & y)
{
    int t = x;
    x = y;
    y = t;
}

const int Ve = 2;
const int V = 2 << Ve; // pocet vlaken, mocnina 2

/* ---------------------------------------------------------------------- */

inline int zacatek_patra (int p) { return (1 << p) - 1; }

inline int cislo_patra (int n)
{
    int t = n+1; // zvetsit o jedna a spocitat bity
    int p = 0;
    while (t > 0)
    {
        p ++;
        t >>= 1; // posun doprava
    }
    return p-1;
}

/* ---------------------------------------------------------------------- */

class Thread : public QThread
{
public:
    int num; // cislo vlakna - jen pro vypisy
    int n; // pocet prvku k trideni

    // parametry pro heapify
    // int i; // pocatecni index
    // int k; // posledni platny index

    // QMutex enter; // signal pro nas, ze muzeme spustit heapify
    // QMutex leave; // signal pro okoli, ze jsem ukoncily heapify
    // QMutex ready; // hotovo

    void heapify (int i, int k)
    {
        cout << "thread " << num << " heapify (" << i << ", " << k << ")" << endl;

        while (2 * i + 1 <= k)
        {
            int v = 2 * i + 1;

            if (v + 1 <= k)
                if (a[v+1] > a[v])
                    v = v + 1;

            if (a[i] < a[v])
            {
                swap (a[i], a[v]);
                i = v;
            }
            else
            {
                i = k+1; // ukonceni cyklu
            }
        }
    }

    void run () override
    {
        int p = cislo_patra (n) - 1; // v nejnizsim patre neni co delat
        while (p >= Ve)
        {
            int z = zacatek_patra (p);
            int m = 1 << (p-Ve);
            z += num * m ; // zacatek sekce
            cout << "patro " << p << " " << z << " " << m << endl;
            for (int u = m-1; u >= 0 ; u--)
            {
                // enter.lock ();
                heapify (z+u, n);
                // leave.unlock ();
            }
            p --; // o patro nahoru
        }
        // ready.unlock ();
    }
};

Thread threads [V]; // pole vlaken

/* ---------------------------------------------------------------------- */

void heapsort (int n)
// setridit a[0] ... a[n-1]
{
    for (int inx = 1; inx <= 1; inx++)
    {
        Thread & t = threads [inx];
        t.num = inx;
        t.n = n;
        // t.enter.lock (); // at pocka
        // t.start ();
        t.run ();
    }


    /*
    for (int i = n-1; i >= 0; i--)
        heapify (i, n-1);

    for (int i = n-1; i > 0; i--)
    {
        swap (a[0], a[i]);
        heapify (0, i-1);
    }
    */
}

/* ---------------------------------------------------------------------- */

void sort ()
{
    srand (time (nullptr));
    for (int i = 0; i < N; i++)
        a[i] = rand() % 10000;

    heapsort (N);

    /*
    bool ok = true;
    for (int i = 0; i < N; i++)
    {
        cout << a[i];
        if (i < N-1 && a[i + 1] < a[i]) { ok = false; cout << " CHYBA"; }
        cout << endl;
    }

    if (ok)
       cout << "O.K." << endl;
    */
}

/* ---------------------------------------------------------------------- */

void numbers (int inx, int n)
{
    cout << "vlakno " << inx << endl;
    for (int p = Ve; p < 6; p++)
    {
        cout << "patro " << p << endl;
        int z = zacatek_patra (p);

        for (int i = 0; i < 1<<(p-Ve) ; i++)
        {
            int c = z + (inx << (p-Ve)) + i;
            cout << "   vrchol " << c << endl;
        }
    }
}

/* ---------------------------------------------------------------------- */

int main(int argc, char *argv[])
{
    // for (int i = 0; i <= 15; i++)
    //    cout << i << "  " << cislo_patra (i) << endl;
    // numbers (1, 63);

    sort ();

    /*
    QApplication a(argc, argv);
    MainWindow w;
    w.show();
    return a.exec();
    */
}
