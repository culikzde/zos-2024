#include "tree.h"
#include "property.h"

#ifdef USE_LLDB
   #include "debugger.h"
#endif

#include <QTreeWidgetItem>
#include <QAction>
#include <QMenu>

#include <iostream>
#include <fstream>
using namespace std;

/* ---------------------------------------------------------------------- */

Tree::Tree (QWidget * parent) :
   QTreeWidget (parent),
   prop (nullptr)
{
   connect (this, &QTreeWidget::itemActivated, this, &Tree::on_item_activated);

   setContextMenuPolicy (Qt::CustomContextMenu);
   connect (this, &QTreeWidget::customContextMenuRequested, this, &Tree::on_context_menu);

}

/* ---------------------------------------------------------------------- */

void Tree::on_item_activated (QTreeWidgetItem * param, int column)
{
   TreeNode * item = dynamic_cast <TreeNode *> (param);
   if (item != nullptr)
   {
      prop->setRowCount (0);

      #ifdef USE_LLDB
         if (item->lldb_object != nullptr)
             displayLldbObject (item->lldb_object, item->type);
      #endif

      #ifdef USE_INVENTOR
         if (item->so_node != nullptr)
            displaySoNode (prop, item->so_node);
      #endif

      if (item->qt_object != nullptr)
         displayQtObject (prop, item->qt_object);

   }
}

/* ---------------------------------------------------------------------- */

void Tree::on_context_menu (const QPoint & pos)
{
#ifdef USE_LLDB
    QTreeWidgetItem * tree_item = itemAt (pos);
    TreeNode * node = dynamic_cast <TreeNode * > (tree_item);

    string name = "";
    if (node != nullptr && node->lldb_object != nullptr)
       name = node->type.GetName ();

    QMenu menu (this);

    QAction * act = new QAction(QString::fromStdString (name), this);
    // connect (newAct, SIGNAL(triggered()), this, SLOT(newDev()));
    menu.addAction (act);

    for (MenuExt ext : menu_ext)
    {
        if (ext.param.IsPointerType() && ext.param.GetPointeeType().GetName() == name)
        {
           QAction * act = new QAction(ext.func.GetName (), this);
           menu.addAction (act);
        }
    }

    QPoint pt (pos);
    menu.exec (mapToGlobal (pos));
#endif
}

/* ---------------------------------------------------------------------- */

#ifdef USE_LLDB
void Tree::displayStrings (QTreeWidgetItem * branch,
                           string name, void * start, lldb::SBType & type)
{
    int field_cnt = type.GetNumberOfFields ();
    for (int field_inx = 0 ; field_inx < field_cnt; field_inx ++)
    {
        lldb::SBTypeMember field = type.GetFieldAtIndex (field_inx);

        string type_name = field.GetType().GetName ();

        if (type_name == "std::string")
        {
           string * str_ptr = (string *) ((char *) start + field.GetOffsetInBytes());
           string value = *str_ptr;
           // prop->addText (name + "." + field.GetName (), QString::fromStdString (value));
           string text = name + "." + field.GetName () + " = " + value;
           QTreeWidgetItem * node = new QTreeWidgetItem (branch);
           node->setText (0, QString::fromStdString (text));
        }
    }
}

void Tree::displayVector (void * start, string field_name, lldb::SBType & vect_type)
{
   cout << field_name << " : vector" << endl;
   /*
   cout << vect_type.GetName() << endl;
   cout << vect_type.GetNumberOfDirectBaseClasses() << endl;
   cout << vect_type.GetDirectBaseClassAtIndex(0).GetName() << endl;

   int arg_cnt = vect_type.GetNumberOfTemplateArguments();
   for (int arg_inx = 0; arg_inx < arg_cnt; arg_inx ++)
   {
       lldb::SBType arg_type = vect_type.GetTemplateArgumentType (arg_inx);
       cout << "ARG " << arg_type.GetName() << endl;
   }
   */

   lldb::SBType elem_type = vect_type.GetTemplateArgumentType (0);
   if (elem_type.IsPointerType ())
   {
       lldb::SBType item_type = elem_type.GetPointeeType();
       cout << "POINTER TO " << item_type.GetName () << endl;

       typedef vector <void*> vect;
       vect * ref = (vect *) start;

       int cnt = ref->size ();
       cout << "SIZE " << cnt << endl;
       prop->addNum (QString::fromStdString(field_name + ".size()"), cnt);

       QTreeWidgetItem * branch = new QTreeWidgetItem (this);
       branch->setText (0, QString::fromStdString (field_name));
       branch->setToolTip (0, QString::number (cnt) + " item(s)");

       // QString field_name_qstr = QString::fromStdString(field_name);
       for (int inx = 0; inx < cnt; inx ++)
       {
          string item_name = field_name + " [" + to_string (inx) + "]";
          void * item_ptr = ref->at (inx);
          displayStrings (branch, item_name, item_ptr, item_type);
       }
   }

   #if 0
   int func_cnt = vect_type.GetNumberOfMemberFunctions ();
   for (int func_inx = 0 ; func_inx < func_cnt; func_inx ++)
   {
       lldb::SBTypeMemberFunction m = vect_type.GetMemberFunctionAtIndex (func_inx);

       #if 0
       cout << "    method " << m.GetName()
            << " : " << m.GetType().GetDisplayTypeName()
            << endl;
       #endif

       string func_name = m.GetName ();
       if (func_name == "size")
       {
          cout << "FUNC " << m.GetMangledName() << " " << m.GetDemangledName() << endl;

          /*
          void * hndl = lt_dlopen (NULL);
          void * addr = lt_dlsym (hndl, m.GetMangledName());
          cout << "HANDLE " << hndl << endl;
          cout << "ADDR " << addr << endl;
          */
          library_functions();
       }
   }
   #endif
}

void Tree::displayField (void * var, lldb::SBTypeMember & field)
{
    cout << "    field " << field.GetName()
         << " : " << field.GetType().GetName()
         << " : " << field.GetType().GetDisplayTypeName()
         << " @ " << field.GetOffsetInBytes () << endl;

    string name = field.GetName ();
    void * start = (char *) var +  field.GetOffsetInBytes ();
    lldb::SBType type = field.GetType();
    string type_name = type.GetName ();

    if (type_name == "std::string")
    {
       string * str_ptr = (string *) start;
       string value = *str_ptr;
       prop->addText (QString::fromStdString (name), QString::fromStdString (value));
    }
    else if (type.IsVectorType())
    {
       lldb::SBType elem = type.GetVectorElementType ();
       cout << "vector of " << elem.GetName () << endl;
       prop->addText ("vector", "");
    }
    else if (type_name.substr (0, 12) == "std::vector<")
    {
       displayVector (start, name, type);
    }
}

/* ---------------------------------------------------------------------- */

void Tree::displayTreeItem (void * var, lldb::SBType type)
{
    cout << "DISPLAY TREE ITEM " << var << " " << type.GetName () << endl;
    TreeNode * node = new TreeNode;
    node->lldb_object = var;
    node->type = type;
    node->setText (0, type.GetName ());
    addTopLevelItem (node);
}

void Tree::displayTreeItem (void * var, string type_name)
{
    #if 0
    lldb::SBTypeList type_list = target.FindTypes (type_name.c_str ());
    int type_cnt = type_list.GetSize();
    cout << "TYPES " << type_cnt << endl;
    lldb::SBType type;
    for (int type_inx = 0 ; type_inx < type_cnt; type_inx ++)
    {
        type = type_list.GetTypeAtIndex (type_inx);
        string name = type.GetName ();
        cout << "TYPE " << name << endl;
    }
    #else
    lldb::SBType type = target.FindFirstType (type_name.c_str ());
    #endif
    displayTreeItem (var, type);
}

/* ---------------------------------------------------------------------- */

QString clean_name (lldb::SBFunction func)
{
    QString s = func.GetName ();

    int i = s.indexOf ("::");
    if (i >= 0) s = s.mid (i+2);

    i = s.indexOf ('(');
    if (i >= 0) s = s.left (i);

    if (s != "") s[0] = s[0].toLower();
    return s;
}

template <class T>
T call (lldb::SBFunction func, void * var)
{
    typedef T (* FuncType) (void *);
    FuncType f = (FuncType) update_address (func.GetStartAddress ().GetFileAddress ());
    return f (var);
}

int call_int (lldb::SBFunction func, void * var)
{
    typedef int (* FuncType) (void *);
    FuncType f = (FuncType) update_address (func.GetStartAddress ().GetFileAddress ());
    return f (var);
}

void Tree::displayConvExt (ConvExt & ext, void * var)
{
    // cout << "CONV EXT " << ext.func.GetName ();
    string result_type = ext.result.GetName ();

    if (result_type == "bool")
    {
        bool value = call <bool> (ext.func, var);
        prop->addBool (clean_name (ext.func), value);
    }
    else if (result_type == "int")
    {
        int value = call_int (ext.func, var);
        prop->addNum (clean_name (ext.func), value);
    }
    else if (result_type == "string")
    {
        string value = call <string> (ext.func, var);
        prop->addText (clean_name (ext.func), QString::fromStdString (value));
    }
    else if (result_type == "QString")
    {
        QString value = call <QString> (ext.func, var);
        prop->addText (clean_name (ext.func), value);
    }
    else if (result_type == "QColor")
    {
        auto value = call <QColor> (ext.func, var);
        prop->addColor (clean_name (ext.func), value);
    }
}

bool is_derived (lldb::SBType type, lldb::SBType base_type)
{
    // cout << "IS DERIVED " << type.GetName () << " from " << base_type.GetName () << endl;
    bool result = (type == base_type);

    int cnt = type.GetNumberOfDirectBaseClasses ();
    // cout << "BASE CLASSES " << cnt << endl;
    for (int inx = 0; inx < cnt && ! result; inx++)
    {
        lldb::SBType cls = type.GetDirectBaseClassAtIndex (inx).GetType ();
        // cout << "COMPARE " << cls.GetName () << endl;
        if (cls == base_type)
        {
            result = true;
        }
        else
        {
            if (is_derived (cls, base_type))
                result = true;
        }
    }

    cnt = type.GetNumberOfVirtualBaseClasses ();
    for (int inx = 0; inx < cnt && ! result; inx++)
    {
        lldb::SBType cls = type.GetVirtualBaseClassAtIndex (inx).GetType ();
        if (cls == base_type)
        {
            result = true;
        }
        else
        {
            if (is_derived (cls, base_type))
                result = true;
        }
    }

    // if (!result) cout << "NOT ";
    // cout << "DERIVED " << type.GetName () << " from " << base_type.GetName () << endl;
    return result;
}

void Tree::displayLldbObject (void * var, lldb::SBType type)
{
    cout << "DISPLAY LLDB " << type.GetName () << endl;

    // prop->clear ();
    prop->setRowCount (0);

    int field_cnt = type.GetNumberOfFields ();
    for (int field_inx = 0 ; field_inx < field_cnt; field_inx ++)
    {
        lldb::SBTypeMember m = type.GetFieldAtIndex (field_inx);
        displayField (var, m);
    }

    // cout << "TYPE " << type.GetName () << endl;
    //  if (type.IsPointerType ())
    {
        // string type_name = type.GetPointeeType ().GetName ();
        string type_name = type.GetName ();
        cout << "TYPE NAME " << type_name << endl;
        for (ConvExt ext : conv_ext)
        {
            // cout << "COMPARE " << ext.param.GetPointeeType ().GetName () << endl;
            // if (ext.param.IsPointerType () && ext.param.GetPointeeType ().GetName () == type_name)
            // if (ext.param.GetPointeeType () == type)
            if (is_derived (type, ext.param.GetPointeeType ()))
            {
                displayConvExt (ext, var);
            }
        }
    }
}

void Tree::displayLldbObject (void * var, string type_name)
{
    #if 0
    lldb::SBTypeList type_list = target.FindTypes (type_name.c_str ());
    int type_cnt = type_list.GetSize();
    cout << "TYPES " << type_cnt << endl;
    lldb::SBType type;
    for (int type_inx = 0 ; type_inx < type_cnt; type_inx ++)
    {
        type = type_list.GetTypeAtIndex (type_inx);
        string name = type.GetName ();
        cout << "TYPE " << name << endl;
    }
    #else
    lldb::SBType type = target.FindFirstType (type_name.c_str ());
    #endif
    displayLldbObject (var, type);
}
#endif // USE_LLDB

/* ---------------------------------------------------------------------- */
